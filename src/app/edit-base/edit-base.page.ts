import { Component, OnInit } from '@angular/core';
import { BaseDaoService } from './../shared/base-dao.service';
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-edit-base',
  templateUrl: './edit-base.page.html',
  styleUrls: ['./edit-base.page.scss'],
})
export class EditBasePage implements OnInit {

  updateBaseForm: FormGroup;
  public_id: any;

  constructor(
    private baseAPI: BaseDaoService,
    private actRoute: ActivatedRoute,
    private router: Router,
    public fb: FormBuilder
  ) {
    this.public_id = this.actRoute.snapshot.paramMap.get('base_id');
  }

  ngOnInit() {
    this.getBaseData(this.public_id);
    this.updateBaseForm = this.fb.group({
      type: [''],
      preview: ['']
    })
  }

  getBaseData(id) {
    this.baseAPI.getBase(id).subscribe(res => {
      this.updateBaseForm.setValue({
        type: res['data']['type'],
        preview: res['data']['preview']
      });
    });
  }

  updateForm() {
    if (!this.updateBaseForm.valid) {
      return false;
    } else {
      this.baseAPI.updateBase(this.public_id, this.updateBaseForm.value)
        .subscribe((res) => {
          console.log(res)
          this.updateBaseForm.reset();
          this.router.navigate(['/home']);
        })
    }
  }

}