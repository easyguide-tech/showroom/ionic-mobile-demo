import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditBasePage } from './edit-base.page';

const routes: Routes = [
  {
    path: '',
    component: EditBasePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditBasePageRoutingModule {}
