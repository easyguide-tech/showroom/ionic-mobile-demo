import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditBasePageRoutingModule } from './edit-base-routing.module';

import { EditBasePage } from './edit-base.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    EditBasePageRoutingModule
  ],
  declarations: [EditBasePage]
})
export class EditBasePageModule {}
