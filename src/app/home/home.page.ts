import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { BaseDaoService } from './../shared/base-dao.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {
  bases: any = [];
  platformname:any;

  constructor(
    private platform: Platform,
    private baseDaoService: BaseDaoService
  ) {
    this.platform.ready().then(()=>{
      
      if (this.platform.is('android')) {
        this.platformname = 'Android'
      } else if (this.platform.is('ios')) {
        this.platformname = 'iOS'
      } else {
        const str: string = this.platform.platforms()[0]
        this.platformname = str[0].toUpperCase() + str.slice(1);
      }
    })
  }

  ngOnInit() {
       
   }

  ionViewDidEnter() {
    this.baseDaoService.getBaseList().subscribe((res) => {
      console.log(res)
      this.bases = res.data.data;
    })
  }

  deleteBase(base, i) {
    if (window.confirm('Do you want to delete base?')) {
      this.baseDaoService.deleteBase(base.public_id)
        .subscribe(() => {
          this.bases.splice(i, 1);
          console.log('Base deleted!')
        }
        )
    }
  }
}