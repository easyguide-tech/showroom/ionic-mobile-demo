import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddBasePageRoutingModule } from './add-base-routing.module';

import { AddBasePage } from './add-base.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AddBasePageRoutingModule
  ],
  declarations: [AddBasePage]
})
export class AddBasePageModule {}
