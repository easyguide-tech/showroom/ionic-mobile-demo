import { Component, OnInit, NgZone } from '@angular/core';
import { BaseDaoService } from './../shared/base-dao.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from "@angular/forms";


@Component({
  selector: 'app-add-base',
  templateUrl: './add-base.page.html',
  styleUrls: ['./add-base.page.scss'],
})

export class AddBasePage implements OnInit {
  baseForm: FormGroup;

  constructor(
    private baseAPI: BaseDaoService,
    private router: Router,
    public fb: FormBuilder,
    private zone: NgZone
  ) {
    this.baseForm = this.fb.group({
      type: [''],
      preview: ['']
    })
  }

  ngOnInit() { }

  onFormSubmit() {
    if (!this.baseForm.valid) {
      return false;
    } else {
      this.baseAPI.addBase(this.baseForm.value)
        .subscribe((res) => {
          this.zone.run(() => {
            console.log(res)
            this.baseForm.reset();
            this.router.navigate(['/home']);
          })
        });
    }
  }
}