import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddBasePage } from './add-base.page';

const routes: Routes = [
  {
    path: '',
    component: AddBasePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddBasePageRoutingModule {}
