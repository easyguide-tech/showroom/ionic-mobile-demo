import { Injectable } from '@angular/core';
import { BaseInterface } from './base.interface';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class BaseDaoService {

  private apiURL = "http://localhost:5000";

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  addBase(base: BaseInterface): Observable<any> {
    return this.http.post<BaseInterface>(this.apiURL + '/bases', base, this.httpOptions)
      .pipe(
        catchError(this.handleError<BaseInterface>('Add Base'))
      );
  }

  getBase(id): Observable<BaseInterface[]> {
    return this.http.get<BaseInterface[]>(this.apiURL + '/bases/' + id)
      .pipe(
        tap(_ => console.log(`Base fetched: ${id}`)),
        catchError(this.handleError<BaseInterface[]>(`Get Base id=${id}`))
      );
  }

  getBaseList(): Observable<{ data: { data: BaseInterface[]}}> {
    return this.http.get<{ data: { data: BaseInterface[]}}>(this.apiURL + '/bases')
      .pipe(
        tap(bases => console.log('Bases fetched!')),
        catchError(this.handleError<any>('Get Bases', []))
      );
  }

  updateBase(id, base: BaseInterface): Observable<any> {
    return this.http.put(this.apiURL + '/bases/' + id, base, this.httpOptions)
      .pipe(
        tap(_ => console.log(`Base updated: ${id}`)),
        catchError(this.handleError<BaseInterface[]>('Update Base'))
      );
  }

  deleteBase(id): Observable<BaseInterface[]> {
    return this.http.delete<BaseInterface[]>(this.apiURL + '/bases/' + id, this.httpOptions)
      .pipe(
        tap(_ => console.log(`Base deleted: ${id}`)),
        catchError(this.handleError<BaseInterface[]>('Delete Base'))
      );
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}