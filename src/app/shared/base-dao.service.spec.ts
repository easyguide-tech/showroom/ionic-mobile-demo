import { TestBed } from '@angular/core/testing';

import { BaseDaoService } from './base-dao.service';

describe('BaseDaoService', () => {
  let service: BaseDaoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BaseDaoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
